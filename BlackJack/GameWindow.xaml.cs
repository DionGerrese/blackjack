﻿using System.Windows;

namespace BlackJack
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        private Game game;
        private Player player;
        private Dealer dealer;

        /// <summary>
        /// GameWindow constructor
        /// </summary>
        public GameWindow()
        {
            InitializeComponent();

            player = new Player();
            dealer = new Dealer();
            game = new Game(this, player, dealer);
            game.UpdateGui();
        }

        /// <summary>
        /// bet button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void betButton_Click(object sender, RoutedEventArgs e)
        {
            game.Bet();
        }

        /// <summary>
        /// hit button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hitButton_Click(object sender, RoutedEventArgs e)
        {
            game.HitHand(player.FirstHand);
        }

        /// <summary>
        /// hit button 2 event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hitButton2_Click(object sender, RoutedEventArgs e)
        {
            game.HitHand(player.SecondHand);
        }

        /// <summary>
        /// fold button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void foldButton_Click(object sender, RoutedEventArgs e)
        {
            game.Fold();
        }

        /// <summary>
        /// font up button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fontUp_Click(object sender, RoutedEventArgs e)
        {
            firstHandBoard.FontSize += 8;
            secondHandBoard.FontSize += 8;
            dealerBoard.FontSize += 8;

            firstHandBoard.FontSize = Utils.Clamp(firstHandBoard.FontSize, 32, 250);
            secondHandBoard.FontSize = Utils.Clamp(secondHandBoard.FontSize, 32, 250);
            dealerBoard.FontSize = Utils.Clamp(dealerBoard.FontSize, 32, 250);

            firstHandField.LineHeight = firstHandBoard.FontSize;
            secondHandField.LineHeight = secondHandBoard.FontSize;
            dealerField.LineHeight = dealerBoard.FontSize;
        }

        /// <summary>
        /// font down button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fontDown_Click(object sender, RoutedEventArgs e)
        {
            firstHandBoard.FontSize -= 8;
            secondHandBoard.FontSize -= 8;
            dealerBoard.FontSize -= 8;

            firstHandBoard.FontSize = Utils.Clamp(firstHandBoard.FontSize, 32, 250);
            secondHandBoard.FontSize = Utils.Clamp(secondHandBoard.FontSize, 32, 250);
            dealerBoard.FontSize = Utils.Clamp(dealerBoard.FontSize, 32, 250);

            firstHandField.LineHeight = firstHandBoard.FontSize;
            secondHandField.LineHeight = secondHandBoard.FontSize;
            dealerField.LineHeight = dealerBoard.FontSize;
        }

        /// <summary>
        /// first hand field size updater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void firstHandBoard_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            firstHandField.Width = firstHandBoard.Width;
            firstHandField.Height = firstHandBoard.Height;
        }

        /// <summary>
        /// second hand field size updater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void secondHandBoard_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            secondHandField.Width = secondHandBoard.Width;
            secondHandField.Height = secondHandBoard.Height;
        }

        /// <summary>
        /// dealer hand field size updater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dealerBoard_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            dealerField.Width = dealerBoard.Width;
            dealerField.Height = dealerBoard.Height;
        }

    }
}