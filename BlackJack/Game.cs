﻿using System.Collections.Generic;
using System.Windows;

namespace BlackJack
{
    public class Game
    {
        private GameWindow gui;
        private Player player;
        private Dealer dealer;
        private Deck deck;

        private bool splitGame = false; // wether the player has split his hand
        private bool playing = false; // wether the game is running or not
        private bool busted = false; // wether a person has busted
        private int decksInGame;
        private int minBet;
        private int bet;

        /// <summary>
        /// Game constructor
        /// </summary>
        /// <param name="gui"></param>
        /// <param name="player"></param>
        /// <param name="dealer"></param>
        public Game(GameWindow gui, Player player, Dealer dealer)
        {
            this.gui = gui;
            this.player = player;
            this.dealer = dealer;

            minBet = 5;
        }
        /// <summary>
        /// Bet Method
        /// </summary>
        public void Bet()
        {
            if (!playing) // check if the game is not running
            {
                bool success = false;

                // check for valid number input
                if (int.TryParse(gui.betAmount.Text, out bet))
                {
                    if (bet < minBet) // check if bet is enough
                        MessageBox.Show("Your bet is below the minimum.\nMake a higher bet.", "Invalid bet input", MessageBoxButton.OK);
                    else if (bet > player.Money) // check if the player has enough money
                        MessageBox.Show("You don't have enough money for that!", "Invalid bet input", MessageBoxButton.OK);
                    else
                        success = true;
                }
                else
                {
                    MessageBox.Show("Please enter a number", "Invalid input", MessageBoxButton.OK); // if the input is not a number
                }

                // start the game if a valid input was given
                if (success)
                {
                    UpdateGui();

                    // reset variables
                    busted = false;
                    playing = true;

                    player.ClearHand();
                    dealer.ClearHand();

                    player.Money -= bet; // remove bet amount from player's money
                    player.AddToHand(dealer.Deal(TakeCards(2)), player.FirstHand); // dealer deals cards to player
                    dealer.TakeCard(TakeCards(1)); // dealer takes a card

                    player.SplitHand(); // split hand method

                    // show the hit second hand button if cards in second hand
                    if (player.GetSecondHandScore() > 0)
                    {
                        gui.hitButton2.Visibility = Visibility.Visible;
                        splitGame = true;
                    }
                    else
                    {
                        gui.hitButton2.Visibility = Visibility.Hidden;
                        splitGame = false;
                    }

                    UpdateGui();
                    CheckScores();
                }
            }
        }
        /// <summary>
        /// Create deck method
        /// </summary>
        /// <returns></returns>
        public Deck CreateDeck()
        {
            deck = new Deck();
            deck.Shuffle();

            return deck;
        }

        /// <summary>
        /// Take cards method: selects cards from the deck to deal
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public List<Card> TakeCards(int amount)
        {
            List<Card> cards = new List<Card>();

            if (decksInGame <= 0 || deck.Cardlist.Count <= amount) // check if a new deck is needed
                CreateDeck();

            for (int i = 0; i <= amount; i++)
            {
                cards.Add(deck.Cardlist[0]); // take the top card
                deck.Cardlist.Remove(deck.Cardlist[0]); // remove the top card from the deck
            }

            return cards;
        }

        /// <summary>
        /// Hit method
        /// </summary>
        /// <param name="hand"></param>
        public void HitHand(List<Card> hand)
        {
            if (playing)
            {
                player.AddToHand(dealer.Deal(TakeCards(1)), hand); // dealer gives the player a card
                UpdateGui();
                CheckScores();
            }
        }

        /// <summary>
        /// Fold method: stops the player's turn
        /// </summary>
        public void Fold()
        {
            if (playing)
            {
                // repeat while the dealer's score is under 17 points
                while (dealer.GetScore() <= 17)
                {
                    dealer.TakeCard(TakeCards(1));
                    UpdateGui();
                    CheckScores();
                }

                playing = false; // end the game
                UpdateGui();
                CheckScores();
            }
        }

        /// <summary>
        /// Check scores method
        /// </summary>
        public void CheckScores()
        {
            int playerScore1 = player.GetFirstHandScore();
            int playerScore2 = player.GetSecondHandScore();
            int dealerScore = dealer.GetScore();

            if (playing)
            {
                if ((!splitGame && playerScore1 > 21) || (splitGame && playerScore1 > 21 && playerScore2 > 21)) // check if the player busted
                {
                    // player loses
                    MessageBox.Show("Player busted, you lose!", "Player Loses", MessageBoxButton.OK);
                    busted = true;
                    playing = false;
                }
                else if (dealerScore > 21) // check if the player busted
                {
                    // player wins
                    MessageBox.Show("Dealer busted, you win!", "Player Wins", MessageBoxButton.OK);
                    player.Money += bet * 2;
                    busted = true;
                    playing = false;
                }
            }
            else if (!busted)
            {
                if (playerScore1 >= dealerScore || playerScore2 >= dealerScore) // check if the player has more points than the dealer
                {
                    // player wins
                    MessageBox.Show("You won!", "Player Wins", MessageBoxButton.OK);
                    player.Money += bet * 2;
                    playing = false;
                }
                else
                {
                    // player loses
                    MessageBox.Show("You lost.", "Player Loses", MessageBoxButton.OK);
                    playing = false;
                }
            }

            UpdateGui();
        }


        /// <summary>
        /// Update gui method: updates the data shown in the gui
        /// </summary>
        public void UpdateGui()
        {
            if (playing)
            {
                gui.firstHandField.Text = player.GetHand(player.FirstHand);
                gui.secondHandField.Text = player.GetHand(player.SecondHand);
                gui.dealerField.Text = dealer.GetHand();
                gui.dealerHandLabel.Content = "Dealer's hand: " + dealer.GetScore().ToString();
                gui.playerHandLabel.Content = "Your hand: " + player.GetFirstHandScore().ToString();

                if (splitGame)
                    gui.playerHandLabel.Content = "Your hand: " + player.GetFirstHandScore().ToString() + "\t" + player.GetSecondHandScore().ToString();
            }
            else if (!playing)
            {
                gui.playerHandLabel.Content = "Your hand: " + string.Empty;
                gui.dealerHandLabel.Content = "Dealer's hand: " + string.Empty;
            }

            gui.minBetLabel.Content = "Min bet: " + minBet.ToString();
            gui.money.Content = player.Money;
        }
    }
}