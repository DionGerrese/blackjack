﻿using System.Collections.Generic;

namespace BlackJack
{
    public class Dealer
    {
        private List<Card> hand;
        private List<Card> cards;

        /// <summary>
        /// Dealer constructor
        /// </summary>
        public Dealer()
        {
            hand = new List<Card>();
            cards = new List<Card>();
        }

        /// <summary>
        /// Deal method: dealer gives cards to the player
        /// </summary>
        /// <param name="takenCards">List of cards to select from</param>
        /// <returns></returns>
        public List<Card> Deal(List<Card> takenCards)
        {
            cards.Clear();

            for (int i = 0; i < takenCards.Count; i++)
            {
                cards.Add(takenCards[i]);
                takenCards.Remove(takenCards[i]);
            }

            return cards;
        }

        /// <summary>
        /// Take card method: dealer takes cards from the deck
        /// </summary>
        /// <param name="takenCards">List of cards to select from</param>
        public void TakeCard(List<Card> takenCards)
        {
            for (int i = 0; i < takenCards.Count; i++)
            {
                hand.Add(takenCards[i]);
                takenCards.Remove(takenCards[i]);
            }
        }

        /// <summary>
        /// GetHand method: returns the cards in the dealer's hand
        /// </summary>
        /// <returns></returns>
        public string GetHand()
        {
            string handText = string.Empty;

            for (int i = 0; i < hand.Count; i++)
                handText += hand[i].Image + " ";

            return handText;
        }

        /// <summary>
        /// GetScore method: returns the dealer's score
        /// </summary>
        /// <returns></returns>
        public int GetScore()
        {
            bool firstDraw = (hand.Count == 2);
            int aces = 0;
            int score = 0;

            // go through the hand and add points to score
            foreach (Card card in hand)
            {
                if (card.Value >= 10)
                    score += 10;
                else if (card.Value == 1)
                    aces++;
                else
                    score += card.Value;
            }

            // add the aces to the score
            for (int i = 0; i < aces; i++)
            {
                if (score <= 11 && aces > 0 && !firstDraw) // check if current score is lower then 12
                {
                    score += 10;
                    aces--;
                }
                else if (firstDraw && aces > 0) // check if it's the first draw
                {
                    score += 11;
                    aces--;
                }
                else if (aces > 0) // check if any aces are left
                {
                    score += 1;
                    aces--;
                }
            }

            return score;
        }

        /// <summary>
        /// Clear hand method
        /// </summary>
        public void ClearHand()
        {
            hand.Clear();
        }
    }
}