﻿using System;
using System.Collections.Generic;

namespace BlackJack
{
    public class Utils
    {
        /// <summary>
        /// random field
        /// </summary>
        public static Random r = new Random();

        /// <summary>
        /// list of spades cards
        /// </summary>
        public static List<string> spades = new List<string>()
        {
            "🂡", "🂢", "🂣", "🂤", "🂥", "🂦", "🂧", "🂨", "🂩", "🂪", "🂫", "🂭", "🂮",
        };

        /// <summary>
        /// list of hearts cards
        /// </summary>
        public static List<string> hearts = new List<string>()
        {
            "🂱", "🂲", "🂳", "🂴", "🂵", "🂶", "🂷", "🂸", "🂹", "🂺", "🂻", "🂽", "🂾",
        };

        /// <summary>
        /// list of diamonds cards
        /// </summary>
        public static List<string> diamonds = new List<string>()
        {
            "🃁", "🃂", "🃃", "🃄", "🃅", "🃆", "🃇", "🃈", "🃉", "🃊", "🃋", "🃍", "🃎",
        };

        /// <summary>
        /// list of clubs cards
        /// </summary>
        public static List<string> clubs = new List<string>()
        {
            "🃑", "🃒", "🃓", "🃔", "🃕", "🃖", "🃗", "🃘", "🃙", "🃚", "🃛", "🃝", "🃞",
        };

        /// <summary>
        /// clamp method
        /// </summary>
        /// <param name="value">value to clamp</param>
        /// <param name="min">minimum value</param>
        /// <param name="max">maximum value</param>
        /// <returns></returns>
        public static double Clamp(double value, double min, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
    }
}