﻿namespace BlackJack
{
    public class Card
    {
        public int Value { get; set; }
        public string Suit { get; set; }
        public string Image { get; set; }

        /// <summary>
        /// Card constructor
        /// </summary>
        /// <param name="value">Card value</param>
        /// <param name="suit">/Card suit type</param>
        /// <param name="image">Card image shown in gui</param>
        public Card(int value, string suit, string image)
        {
            Value = value;
            Suit = suit;
            Image = image;
        }
    }
}