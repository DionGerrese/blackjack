﻿using System.Collections.Generic;

namespace BlackJack
{
    public class Deck
    {
        /// <summary>
        /// List of cards in deck
        /// </summary>
        public List<Card> Cardlist { get; set; }

        /// <summary>
        /// Deck constructor
        /// </summary>
        public Deck()
        {
            Cardlist = new List<Card>();

            // add 52 cards to the deck
            for (int i = 1; i <= 13; i++)
                Cardlist.Add(new Card(i, "Clubs", Utils.clubs[i-1]));
            for (int i = 1; i <= 13; i++)
                Cardlist.Add(new Card(i, "Diamonds", Utils.diamonds[i-1]));
            for (int i = 1; i <= 13; i++)
                Cardlist.Add(new Card(i, "Hearts", Utils.hearts[i-1]));
            for (int i = 1; i <= 13; i++)
                Cardlist.Add(new Card(i, "Spades", Utils.spades[i-1]));
        }

        /// <summary>
        /// Shuffle method: shuffles the deck
        /// </summary>
        public void Shuffle()
        {
            List<Card> shuffled = new List<Card>();

            for (int i = 0; i < Cardlist.Count; i++)
            {
                int selection = Utils.r.Next(0, Cardlist.Count); // select a random card from the deck

                // move the selected card to the temp list
                shuffled.Add(Cardlist[selection]);
                Cardlist.Remove(Cardlist[selection]);
            }

            // move all cards back to the cardlist
            Cardlist = shuffled;
        }
    }
}