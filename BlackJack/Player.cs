﻿using System.Collections.Generic;
using System.Windows;

namespace BlackJack
{
    public class Player
    {
        /// <summary>
        /// The player's money
        /// </summary>
        public int Money { get { return money; } set { money = value; } }
        /// <summary>
        /// The initial hand
        /// </summary>
        public List<Card> FirstHand { get { return firstHand; } }
        /// <summary>
        /// Hand used for split games
        /// </summary>
        public List<Card> SecondHand { get { return secondHand; } }

        private int money;
        private List<Card> firstHand;
        private List<Card> secondHand;

        /// <summary>
        /// Player constructor
        /// </summary>
        public Player()
        {
            money = 50;
            firstHand = new List<Card>();
            secondHand = new List<Card>();
        }

        /// <summary>
        /// GetHand method: returns the cards in the dealer's hand
        /// </summary>
        /// <param name="hand">The hand to get the cards from</param>
        /// <returns></returns>
        public string GetHand(List<Card> hand)
        {
            string handText = string.Empty;

            if (hand.Count > 0)
            {
                for (int i = 0; i < hand.Count; i++)
                    handText += hand[i].Image + " ";
            }

            return handText;
        }

        /// <summary>
        /// Add to hand method: add cards to the selected hand
        /// </summary>
        /// <param name="cards">the cards to add to the hand</param>
        /// <param name="hand">the hand to add cards to</param>
        public void AddToHand(List<Card> cards, List<Card> hand)
        {
            foreach (Card card in cards)
                hand.Add(card);
        }

        /// <summary>
        /// GetScore method for the first hand
        /// </summary>
        /// <returns></returns>
        public int GetFirstHandScore()
        {
            bool firstDraw = (firstHand.Count == 2);
            int aces = 0;
            int score = 0;

            // check if the first two cards are the same
            if (firstDraw && firstHand[0] == firstHand[1])
                SplitHand();

            // go through the hand and add points to score
            foreach (Card card in firstHand)
            {
                if (card.Value >= 10)
                    score += 10;
                else if (card.Value == 1)
                    aces++;
                else
                    score += card.Value;
            }

            // add the aces to the score
            for (int i = 0; i < aces; i++)
            {
                if (score <= 11 && aces > 0 && !firstDraw)
                {
                    score += 10;
                    aces--;
                }
                else if (firstDraw && aces > 0)
                {
                    score += 11;
                    aces--;
                }
                else if (aces > 0)
                {
                    score += 1;
                    aces--;
                }
            }

            return score;
        }

        /// <summary>
        /// GetScore method for the second hand
        /// </summary>
        /// <returns></returns>
        public int GetSecondHandScore()
        {
            int aces = 0;
            int score = 0;

            // go through the hand and add points to score
            foreach (Card card in secondHand)
            {
                if (card.Value >= 10)
                    score += 10;
                else if (card.Value == 1)
                    aces++;
                else
                    score += card.Value;
            }

            // add the aces to the score
            while (aces > 0)
            {
                if (score <= 11 && aces > 0)
                {
                    score += 10;
                    aces--;
                }
                else if (aces > 0)
                {
                    score += 1;
                    aces--;
                }
            }

            return score;
        }

        /// <summary>
        /// SplitHand method
        /// </summary>
        public void SplitHand()
        {
            if (firstHand[0].Value == firstHand[1].Value)
            {
                if (MessageBox.Show("Do you want to split your hand?", "Splitting", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    secondHand.Add(firstHand[1]);
                    firstHand.Remove(firstHand[1]);
                }
            }
        }

        /// <summary>
        /// Clear hand method
        /// </summary>
        public void ClearHand()
        {
            firstHand.Clear();
            secondHand.Clear();
        }
    }
}